import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor() { }
  getCategory(){
    return fetch("../assets/category.json")
      .then(response => response.json())
      .then(data => data)
      .catch((error) => console.log(error))
  }
}
