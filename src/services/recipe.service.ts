import { Injectable } from '@angular/core';
import { Recipe } from '../types/Recipe';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  constructor() {}
  getRecipe(){
    return fetch("../assets/recipe.json")
      .then(response => response.json())
      .then(data => data)
      .catch((error) => console.error(error));
  }
}
