export type Category = {
  categoryButtonId: number;
  categoryButtonText: string;
};
