export type Recipe = {
    recipeId: number;
    recipeTitle: string;
    recipeImg: string;
    recipeFavorite: boolean;
    recipeDescription: string;
    recipeCategories: string[];
    recipeDuration: number;
    recipeLevel: string;
}