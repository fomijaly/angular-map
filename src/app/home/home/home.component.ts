import { Component } from '@angular/core';
import { NavigationComponent } from '../navigation/navigation.component';
import { RecipeCardComponent } from '../../recipe/recipe-card/recipe-card.component';
import { RecipeService } from '../../../services/recipe.service';
import { Recipe } from '../../../types/Recipe';
import { CategoryComponent } from '../../shared/category/category.component';
import { CategoryService } from '../../../services/category.service';
import { Category } from '../../../types/Category';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [NavigationComponent, RecipeCardComponent, CategoryComponent],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css',
})
export class HomeComponent {
  allRecipes: [] = [];
  filter = { RegisterRecipeName: '', category: '' };

  constructor(
    public recipeService: RecipeService,
    public categoryService: CategoryService
  ) {}

  ngOnInit(): void {
    this.recipeService.getRecipe().then((data) => {
      this.allRecipes = data;
      this.setData();
    });
    this.categoryService.getCategory().then((data) => {
      this.categories = data;
    });
  }

  setData() {
    this.recipes = [...this.allRecipes];
  }

  recipes?: Recipe[];
  categories?: Category[];

  submitForm(e: Event) {
    e.preventDefault();
    this.filterRecipes();
  }

  filterRecipes() {
    if (this.filter.RegisterRecipeName.trim() !== '') {
      this.recipes = this.allRecipes.filter((recipe: Recipe) =>
        recipe.recipeTitle
          .toLowerCase()
          .includes(this.filter.RegisterRecipeName.toLowerCase())
      );
    } else {
      this.setData();
    }
  }

  filterCategory(category: string) {
    this.filter.category = category;
    if (this.filter.category !== '') {
      this.recipes = this.allRecipes.filter((recipe: Recipe) =>
        recipe.recipeCategories.includes(this.filter.category)
      );
    }
  }

  refreshRecipeList() {
    this.filter.RegisterRecipeName = '';
    this.filter.category = '';
    this.setData();
  }

  categoryInRecipe(category: Category) {
    return this.allRecipes.some((recipe: Recipe) =>
      recipe.recipeCategories.includes(category.categoryButtonText)
    );
  }
}
