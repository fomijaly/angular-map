import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-button-category',
  standalone: true,
  imports: [],
  templateUrl: './button-category.component.html',
  styleUrl: './button-category.component.css'
})
export class ButtonCategoryComponent {
  @Input() categoryText:string = "";
  @Input() categoryStyle:string = "";
}
