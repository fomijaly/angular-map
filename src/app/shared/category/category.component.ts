import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Category } from '../../../types/Category';

@Component({
  selector: 'app-category',
  standalone: true,
  imports: [],
  templateUrl: './category.component.html',
  styleUrl: './category.component.css',
})
export class CategoryComponent {
  @Input() categoryButtonText: string = '';
  @Input() category?: Category | undefined;
  isActive: Boolean = false;
  @Output() categorySelected = new EventEmitter<void>();

  selectCategory() {
    this.isActive = true;
    this.categorySelected.emit();
  }
}
