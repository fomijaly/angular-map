import { Routes } from '@angular/router';
import { HomeComponent } from './home/home/home.component';
import { MapComponent } from './map/map-details/map.component';
import { RecipeDetailsComponent } from './recipe/recipe-details/recipe-details.component';

export const routes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'explore', component: MapComponent},
    {path: 'recipe/:recipeId', component: RecipeDetailsComponent}
];
