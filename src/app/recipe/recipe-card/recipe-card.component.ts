import { Component, Input } from '@angular/core';
import { ButtonComponent } from '../../shared/button/button.component';
import { ButtonCategoryComponent } from '../../shared/button-category/button-category.component';
import { Recipe } from '../../../types/Recipe';

@Component({
  selector: 'app-recipe-card',
  standalone: true,
  imports: [ButtonCategoryComponent, ButtonComponent],
  templateUrl: './recipe-card.component.html',
  styleUrl: './recipe-card.component.css',
})
export class RecipeCardComponent {
  @Input() recipeTitle: String = '';
  @Input() recipeImg: String = '';
  @Input() recipeFavorite: Boolean = false;
  @Input() recipe?: Recipe;
  @Input() recipeUrl: String = '';
}
