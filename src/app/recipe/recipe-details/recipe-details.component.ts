import { Component, Input } from '@angular/core';
import { Recipe } from '../../../types/Recipe';
import { RecipeService } from '../../../services/recipe.service';
import { ActivatedRoute } from '@angular/router';
import { ParamMap } from '@angular/router';

@Component({
  selector: 'app-recipe-details',
  standalone: true,
  imports: [],
  templateUrl: './recipe-details.component.html',
  styleUrl: './recipe-details.component.css',
})
export class RecipeDetailsComponent {
  recipeId?: number;

  constructor(public recipeService: RecipeService, private route: ActivatedRoute){}

  ngOnInit():void{
    this.recipeService.getRecipe().then((data) => {
      this.recipe = data;
      console.log(this.recipe)
    })

    const paramMap: ParamMap | null = this.route.snapshot.paramMap;
    if (paramMap !== null) {
      const recipeId = paramMap.get('recipeId');
      if (recipeId !== null) {
        this.recipeId = +recipeId;
      }
    } 
  }
  @Input() recipe?: Recipe[];
}
