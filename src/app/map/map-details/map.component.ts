import { Component } from '@angular/core';
import { ChartComponent } from '../chart/chart.component';
import { NavigationComponent } from '../../home/navigation/navigation.component';

@Component({
  selector: 'app-map',
  standalone: true,
  imports: [
    ChartComponent,
    NavigationComponent
  ],
  templateUrl: './map.component.html',
  styleUrl: './map.component.css'
})
export class MapComponent {

}
