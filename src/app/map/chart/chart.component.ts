import { Component, Inject, NgZone, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

// amCharts imports
import * as am5 from '@amcharts/amcharts5';
import * as am5map from '@amcharts/amcharts5/map';
import am5themes_Animated from '@amcharts/amcharts5/themes/Animated';
import am5geodata_worldLow from "@amcharts/amcharts5-geodata/worldLow";
import am5geodata_lang_FR from "@amcharts/amcharts5-geodata/lang/FR"; 

@Component({
  selector: 'app-chart',
  standalone: true,
  imports: [],
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})

export class ChartComponent {
  private root!: am5.Root;

  constructor(@Inject(PLATFORM_ID) private platformId: Object, private zone: NgZone) {}

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {
      let root = am5.Root.new("chartdiv");

      root.setThemes([am5themes_Animated.new(root)]);

      root.setThemes([
        am5themes_Animated.new(root)
      ]);
      
      
      // Create the map chart
      // https://www.amcharts.com/docs/v5/charts/map-chart/
      let chart = root.container.children.push(am5map.MapChart.new(root, {
        projection: am5map.geoNaturalEarth1()
      }));
      
      
      // Create main polygon series for countries
      // https://www.amcharts.com/docs/v5/charts/map-chart/map-polygon-series/
      let polygonSeries = chart.series.push(am5map.MapPolygonSeries.new(root, {
        geoJSON: am5geodata_worldLow,
        geodataNames: am5geodata_lang_FR,
        fill: am5.color(0xffffff),
        stroke: am5.color(0xCAD2D3),
        exclude: ["AQ"]
      }));
      
      polygonSeries.mapPolygons.template.setAll({
        stroke: am5.color(0x000000),
        strokeWidth: .8,
        tooltipText: "{name}",
        toggleKey: "active",
        interactive: true
      });

      polygonSeries.mapPolygons.template.events.on("click", (e) => {
        // polygonSeries.mapPolygons.push
        let newValue = e.target.uid;
        console.log("new", newValue)
        console.log("item", e.target._settings)
      })
      
      polygonSeries.mapPolygons.template.states.create("hover", {
        fill: root.interfaceColors.set("grid", am5.color(0xB7B9A6))
      });
      
      polygonSeries.mapPolygons.template.states.create("active", {
        fill: root.interfaceColors.set("grid", am5.color(0x676952))
      });
      
      // Make stuff animate on load
      chart.appear(1000, 100);

      this.root = root;
    });
  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.root) {
        this.root.dispose();
      }
    });
  }
}